/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

/*
 * minimalistic preferences
 */

import static android.os.Environment.getExternalStorageState;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

public class MyPreferenceActivity extends AppCompatActivity
{
    /** @noinspection Convert2Lambda*/
    static public class MySettingsFragment extends PreferenceFragmentCompat
    {
        private boolean resetPaths(Context context)
        {
            boolean ret = false;
            if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
            {
                UserSettings.setContextIfNecessary(context);

                // get current values
                String currMusicBasePath = UserSettings.getString(UserSettings.PREF_MUSIC_BASE_PATH);
                String currDatabasePath = UserSettings.getString(UserSettings.PREF_DATA_BASE_PATH);
                // get new default values
                String defaultMusicBasePath = Utilities.getDefaultMusicBasePath(context);
                String defaultSharedDbBasePath = Utilities.getDefaultSharedDbBasePath(context);
                // check if there are changes
                if (Utilities.stringsDiffer(currMusicBasePath, defaultMusicBasePath))
                {
                    UserSettings.removeVal(UserSettings.PREF_MUSIC_BASE_PATH);
                    UserSettings.getAndPutString(UserSettings.PREF_MUSIC_BASE_PATH, defaultMusicBasePath);
                    ret = true;
                }
                if (Utilities.stringsDiffer(currDatabasePath, defaultSharedDbBasePath))
                {
                    UserSettings.removeVal(UserSettings.PREF_DATA_BASE_PATH);
                    UserSettings.getAndPutString(UserSettings.PREF_DATA_BASE_PATH, defaultSharedDbBasePath);
                    ret = true;
                }
            }
            return ret;
        }

        private void handleRestorePaths()
        {
            Preference restoreDefaultPaths = findPreference("prefRestoreDefaultPaths");
            if (restoreDefaultPaths != null)
            {
                restoreDefaultPaths.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
                {
                    @Override
                    public boolean onPreferenceClick(@NonNull Preference preference)
                    {
                        FragmentActivity activity = getActivity();
                        if (activity != null)
                        {
                            if (resetPaths(activity))
                            {
                                Toast.makeText(activity, R.string.str_paths_updated, Toast.LENGTH_LONG).show();
                                /* unfortunately there is no way to update elements from stored preferences
                                Preference pref = findPreference("prefMusicBasePath");
                                String key = pref.getKey();
                                pref.setKey(key);  // update!
                                pref = findPreference("prefDataBasePath");
                                key = pref.getKey();
                                pref.setKey(key);  // update!
                                */

                                setPreferenceScreen(null);
                                addPreferencesFromResource(R.xml.preferences);
                                handleRestorePaths();
                            }
                            else
                            {
                                Toast.makeText(activity, R.string.str_paths_unchanged, Toast.LENGTH_LONG).show();
                            }

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                });
            }
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.preferences, rootKey);
            handleRestorePaths();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MySettingsFragment())
                .commit();
    }
}
